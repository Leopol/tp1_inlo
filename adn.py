#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 14:25:30 2021

@author: Leopoldine

TP_INLO_GIT

"""

# Test de fonction
def is_valid_test(adn):
    letters = ("a","t","g","c")
    for i in adn :
        if i in letters :
            print(True)
        else :
            print(False)
            
is_valid_test('atgcgtattgtcctg')

# Fonctions fonctionnelles
import re

def is_valid(adn):
    return bool(re.search(r"^[atgc]+$",adn))

def get_valid_adn():
    while not is_valid(str(input("Please enter an DNA string :"))):
        print("It's not a DNA sequence")
    print("We can analyze your DNA sequence")
